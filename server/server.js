/* eslint-disable complexity */
import http from 'http';
import url from 'url';
import uuidv4 from 'uuid/v4';
import mysql from 'mysql2/promise';
import { getChangedValues, getDateTime, createTable } from './utils/utils.js';
import { JSDOM } from 'jsdom';
import nodemailer from 'nodemailer';

export default class Server {
    httpServer = null;
    errorCodes = {
        sqlError: 'SQL_ERROR',
        noSuchCommand: 'NO_SUCH_COMMAND',
        noSuchServerCommand: 'NO_SUCH_SERVER_COMMAND',
        noSuchDbError: 'NO_SUCH_DB_ERROR',
        noSuchTableError: 'NO_SUCH_TABLE_ERROR',
        accessDeniedError: 'ACCESS_DENIED_ERROR',
        noDbConnection: 'NO_DB_CONNECTION_ERROR',
        noChanges: 'DID_NOT_RECEIVE_CHANGES_ERROR',
        emptyKeyField: 'EMPTY_KEY',
        emptyOldKeyField: 'EMPTY_OLD_KEY',
        keyAlreadyExists: 'KEY_ALREADY_EXISTS',
        keyDoesNotExist: 'KEY_DOES_NOT_EXISTS',
        keyAdded: 'KEY_ADDED',
        keyDeleted: 'KEY_DELETED',
        sameValue: 'SAME_VALUE',
    };
    successCodes = {
        saveDone: 'SAVED_SUCCESSFULLY',
        saveAndMergeDone: 'SAVED_AND_MERGE_SUCCESSFULLY',
    };
    dbSessions = {};
    contentTypes = {
        json: { 'Content-Type': 'application/json' },
    };
    httpServerPort = 1010;
    postProcessors = {};
    getProcessors = {};
    dom = new JSDOM(`<!DOCTYPE html>`);
    transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: 'test.deveducation@gmail.com',
            pass: 'DevEducation',
        },
    });

    initialize = async () => {
        // await this.connectToDb();
        this.runHttpServer();
        this.initRequestProcessors();
    };

    connectToDb = async (settings) => {
        try {
            const dbConnection = await mysql.createConnection(settings);
            this.setConnectionErrorHandler(dbConnection);
            return dbConnection;
        } catch (err) {
            console.error(`DB connection failed: ${err.code}`);
            return err.code;
        }
    };

    setConnectionErrorHandler = (dbConnection) => {
        dbConnection.on('error', async (err) => await this.dbErrorHandler(err));
    };

    dbErrorHandler = async (error) => {
        if (error.code === 'PROTOCOL_CONNECTION_LOST') {
            console.log(error);
            // this.dbConnection.destroy();
            // this.dbConnection = null;
            // await this.connectToDb();
        }
    };

    runHttpServer = () => {
        // if (!this.dbConnection) {
        //     console.error(new Error(`Failed to create server: DB connection is not created.`));
        //     return false;
        // }

        this.httpServer = http.createServer((req, res) =>
            this.processRequest(req, res)
        );
        this.httpServer.listen(this.httpServerPort, this.httpServerListenLog);
    };

    initRequestProcessors = () => {
        // !this.dbConnection ||
        if (!this.httpServer) {
            console.error(
                `Failed to init processors: DB connection or HTTP server is not created.`
            );

            return false;
        }

        this.postProcessors = {
            auth: this.loginProcessor,
            logout: this.logoutProcessor,
            edit: this.editProcessor,
            'edit-and-merge': this.editAndMergeProcessor,
            'add-key': this.addKeysProcessor,
            'update-key': this.updateKeysProcessor,
            'delete-key': this.deleteKeysProcessor,
        };

        this.getProcessors = {
            persons: this.personsProcessor,
            person: this.personByIdProcessor,
            logs: this.logsProcessor,
        };
    };

    httpServerListenLog = () => {
        console.log(
            new Date() + '\nServer is listening on port ' + this.httpServerPort
        );
    };

    processRequest = async (req, res) => {
        const requestQuery = url.parse(req.url, true);
        const requestTarget = requestQuery.pathname.substring(1);
        const requestParams = requestQuery.query;

        console.log(requestTarget);

        switch (req.method) {
            case 'POST':
                let body = '';

                req.on('data', (data) => {
                    body += data;

                    if (body.length > 1e6) {
                        req.connection.destroy();
                    }
                });

                req.on('end', async () => {
                    const postArgs = JSON.parse(body);

                    if (this.postProcessors[requestTarget]) {
                        const responseContentPost = await this.postProcessors[
                            requestTarget
                        ](postArgs);
                        this.sendResponse(
                            res,
                            this.contentTypes.json,
                            JSON.stringify(responseContentPost)
                        );
                    } else {
                        this.sendResponse(
                            res,
                            this.contentTypes.json,
                            JSON.stringify(
                                this.getErrorResponseObject(
                                    'noSuchCommand',
                                    null
                                )
                            )
                        );
                    }
                });
                break;

            case 'GET':
                if (this.getProcessors[requestTarget]) {
                    const responseContentGet = await this.getProcessors[
                        requestTarget
                    ](requestParams);
                    this.sendResponse(
                        res,
                        this.contentTypes.json,
                        JSON.stringify(responseContentGet)
                    );
                } else {
                    this.sendResponse(
                        res,
                        this.contentTypes.json,
                        JSON.stringify(
                            this.getErrorResponseObject('noSuchCommand', null)
                        )
                    );
                }
                break;

            default:
                this.sendResponse(
                    res,
                    this.contentTypes.json,
                    JSON.stringify(
                        this.getErrorResponseObject('noSuchServerCommand', null)
                    )
                );
        }
    };

    getErrorResponseObject = (command, errorData) => {
        return {
            isError: true,
            code: this.errorCodes[command],
            data: errorData || null,
        };
    };

    getAuthResponseObject = (connectionId, isAuthorized, permissionWrite) => {
        return {
            isAuthorized,
            permissionWrite,
            id: connectionId,
        };
    };

    getSuccessResponseObject = (command, successData) => {
        return {
            isError: false,
            code: this.successCodes[command],
            data: successData || null,
        };
    };

    sendResponse = (response, header, data) => {
        response.setHeader('Access-Control-Allow-Origin', '*');
        response.setHeader('Access-Control-Request-Method', '*');
        response.setHeader('Access-Control-Allow-Methods', '*');
        response.setHeader('Access-Control-Allow-Headers', '*');
        response.writeHead(200, header);
        response.write(data);
        response.end();
    };

    loginProcessor = async ({ connectionId, ...rest }) => {
        if (this.dbSessions[connectionId]) {
            const { permissionWrite } = this.dbSessions[connectionId];
            return this.getAuthResponseObject(
                connectionId,
                true,
                permissionWrite
            );
        }

        return await this.registrateNewConnection(rest);
    };

    registrateNewConnection = async ({ name, pass, db, table }) => {
        const newConnectionId = uuidv4();
        const connection = await this.connectToDb({
            user: name,
            password: pass,
            database: db,
        });

        if (connection === 'ER_ACCESS_DENIED_ERROR') {
            return this.getErrorResponseObject('accessDeniedError');
        }

        if (connection === 'ER_BAD_DB_ERROR') {
            return this.getErrorResponseObject('noSuchDbError');
        }

        if (connection === 'ECONNREFUSED') {
            return this.getErrorResponseObject('noDbConnection');
        }

        const isTableExists = await this.isTableExists(connection, table);

        if (!isTableExists) {
            return this.getErrorResponseObject('noSuchTableError');
        }

        const [[userGrant]] = await connection.execute(
            `SHOW GRANTS FOR '${name}'`
        );
        const privileges = userGrant[`Grants for ${name}@%`].indexOf(
            'ALL PRIVILEGES'
        );
        const permissionWrite = privileges != -1;

        this.dbSessions[newConnectionId] = {
            user: name,
            permissionWrite,
            table,
            connection,
        };

        return this.getAuthResponseObject(
            newConnectionId,
            true,
            permissionWrite
        );
    };

    isTableExists = async (dbConnection, table) => {
        let res;
        try {
            [res] = await dbConnection.execute(`SHOW TABLES LIKE '${table}'`);
        } catch (err) {
            res = this.getErrorResponseObject('sqlError', err.message);
        }
        return res.isError ? res : res.length;
    };

    logoutProcessor = ({ connectionId }) => {
        this.dbSessions[connectionId].connection.destroy();
        delete this.dbSessions[connectionId];

        return this.getAuthResponseObject(null, false);
    };

    personsProcessor = async ({ id, version }) => {
        const { table, connection } = this.dbSessions[id];

        return await this.getPersons(connection, table, version);
    };

    personByIdProcessor = async ({ id, personId, version }) => {
        const { table, connection } = this.dbSessions[id];

        return await this.getPerson(connection, personId, version, table);
    };

    logsProcessor = async ({ id, version }) => {
        const { table, connection } = this.dbSessions[id];
        let res;

        try {
            [res] = await connection.execute(
                `SELECT * FROM ${table}_logs WHERE version='${version}'`
            );
        } catch (err) {
            res = this.getErrorResponseObject(err.code, err.message);
        }

        return res;
    };

    editProcessor = async function ({
        id,
        personId,
        version,
        searchingValue,
        ...personFields
    }) {
        const { table, connection, user } = this.dbSessions[id];

        const personFieldsDB = await this.getPerson(
            connection,
            personId,
            version,
            table
        );
        if (personFieldsDB.isError) {
            return personFieldsDB;
        }

        const changedValues = getChangedValues(personFields, personFieldsDB);
        if (!changedValues.length) {
            return this.getErrorResponseObject('noChanges');
        }

        const resUpdatePerson = await this.updatePerson(
            connection,
            table,
            personId,
            version,
            personFields
        );
        if (resUpdatePerson.isError) {
            return resUpdatePerson;
        }

        const logsRows = changedValues.map((item) => ({
            ...item,
            user,
            dateTime: getDateTime(),
            version,
            searchingValue,
        }));

        const resWriteLogs = await this.writeLogs(connection, table, logsRows);
        if (resWriteLogs.isError) {
            return resWriteLogs;
        }

        const resBroadcastEmails = await this.broadcastEmails(
            connection,
            table,
            logsRows
        );
        if (resBroadcastEmails.isError) {
            return resBroadcastEmails;
        }

        return this.getSuccessResponseObject('saveDone');
    };

    editAndMergeProcessor = async ({
        id,
        personId,
        version,
        searchingValue,
        ...personFields
    }) => {
        const { table, connection, user } = this.dbSessions[id];

        const personFieldsDB = await this.getPerson(
            connection,
            personId,
            version,
            table
        );
        if (personFieldsDB.isError) {
            return personFieldsDB;
        }

        const changedValues = getChangedValues(personFields, personFieldsDB);
        if (!changedValues.length) {
            return this.getErrorResponseObject('noChanges');
        }

        const columns = await this.getColumnsNames(connection, table);
        if (columns.isError) {
            return columns;
        }

        let logsRowsAll = [];
        for (const version of columns) {
            const resUpdatePerson = await this.updatePerson(
                connection,
                table,
                personId,
                version,
                personFields
            );
            if (resUpdatePerson.isError) {
                return resUpdatePerson;
            }

            const logsRows = changedValues.map((item) => ({
                ...item,
                user,
                dateTime: getDateTime(),
                version,
                searchingValue,
            }));
            logsRowsAll = [...logsRowsAll, ...logsRows];
        }

        const resWriteLogs = await this.writeLogs(
            connection,
            table,
            logsRowsAll
        );
        if (resWriteLogs.isError) {
            return resWriteLogs;
        }

        const resBroadcastEmails = await this.broadcastEmails(
            connection,
            table,
            logsRowsAll
        );
        if (resBroadcastEmails.isError) {
            return resBroadcastEmails;
        }

        return this.getSuccessResponseObject('saveAndMergeDone');
    };

    addKeysProcessor = async ({ id, version, key, isMerge }) => {
        if (!key) {
            return this.getErrorResponseObject('emptyKeyField');
        }

        const { table, connection } = this.dbSessions[id];

        if (isMerge) {
            const columns = await this.getColumnsNames(connection, table);
            if (columns.isError) {
                return columns;
            }

            for (const column of columns) {
                const resAddKeysPersons = await this.addKeysPersons(
                    connection,
                    table,
                    column,
                    key,
                    isMerge
                );
                if (resAddKeysPersons.isError) {
                    return resAddKeysPersons;
                }
            }
        } else {
            const resAddKeysPersons = await this.addKeysPersons(
                connection,
                table,
                version,
                key,
                isMerge
            );
            if (resAddKeysPersons.isError) {
                return resAddKeysPersons;
            }
        }

        const persons = await this.getPersons(connection, table, version);
        if (persons.isError) {
            return persons;
        }

        return persons;
    };

    updateKeysProcessor = async ({ id, version, key, isMerge, oldKey }) => {
        if (!oldKey) {
            return this.getErrorResponseObject('emptyOldKeyField');
        }

        if (!key) {
            return this.getErrorResponseObject('emptyKeyField');
        }

        if (key === oldKey) {
            return this.getErrorResponseObject('sameValue');
        }

        const { table, connection } = this.dbSessions[id];

        if (isMerge) {
            const columns = await this.getColumnsNames(connection, table);
            if (columns.isError) {
                return columns;
            }

            for (const column of columns) {
                const resUpdateKeysPersons = await this.updateKeysPersons(
                    connection,
                    table,
                    column,
                    key,
                    oldKey,
                    isMerge
                );
                if (resUpdateKeysPersons.isError) {
                    return resUpdateKeysPersons;
                }
            }
        } else {
            const resUpdateKeysPersons = await this.updateKeysPersons(
                connection,
                table,
                version,
                key,
                oldKey,
                isMerge
            );
            if (resUpdateKeysPersons.isError) {
                return resUpdateKeysPersons;
            }
        }

        const persons = await this.getPersons(connection, table, version);
        if (persons.isError) {
            return persons;
        }

        return persons;
    };

    deleteKeysProcessor = async ({ id, version, key, isMerge }) => {
        if (!key) {
            return this.getErrorResponseObject('emptyKeyField');
        }

        const { table, connection } = this.dbSessions[id];

        if (isMerge) {
            const columns = await this.getColumnsNames(connection, table);
            if (columns.isError) {
                return columns;
            }

            for (const column of columns) {
                const res = await this.deleteKeysPersons(
                    connection,
                    table,
                    column,
                    key
                );
                if (res.isError) {
                    return res;
                }
            }
        } else {
            const res = await this.deleteKeysPersons(
                connection,
                table,
                version,
                key
            );
            if (res.isError) {
                return res;
            }
        }

        const persons = await this.getPersons(connection, table, version);
        if (persons.isError) {
            return persons;
        }

        return persons;
    };

    addKeysPersons = async (connection, table, version, key, isMerge) => {
        const persons = await this.getPersons(connection, table, version);
        if (persons.isError) {
            return persons;
        }

        if (persons[0].hasOwnProperty(key) && !isMerge) {
            return this.getErrorResponseObject(
                'keyAlreadyExists',
                `${version} already has key ${key}`
            );
        }

        persons.forEach((item) => item[key] = '');

        for (const person of persons) {
            const resUpdatePerson = await this.updatePerson(
                connection,
                table,
                person.id,
                version,
                person
            );
            if (resUpdatePerson.isError) {
                return resUpdatePerson;
            }
        }

        return this.getSuccessResponseObject('keyAdded');
    };

    updateKeysPersons = async (
        connection,
        table,
        version,
        key,
        oldKey,
        isMerge
    ) => {
        const persons = await this.getPersons(connection, table, version);
        if (persons.isError) {
            return persons;
        }

        if (!isMerge) {
            if (persons[0].hasOwnProperty(key)) {
                return this.getErrorResponseObject(
                    'keyAlreadyExists',
                    `${version} already has key ${key}`
                );
            }
            if (!persons[0].hasOwnProperty(oldKey)) {
                return this.getErrorResponseObject(
                    'keyDoesNotExist',
                    `${version} hasn't key ${key}`
                );
            }
        }

        if (!persons[0].hasOwnProperty(oldKey)) {
            persons.forEach((item) => {
                item[key] = '';
            });
        } else {
            persons.forEach((item) => {
                item[key] = item[oldKey];
                delete item[oldKey];
            });
        }

        for (const person of persons) {
            const resUpdatePerson = await this.updatePerson(
                connection,
                table,
                person.id,
                version,
                person
            );
            if (resUpdatePerson.isError) {
                return resUpdatePerson;
            }
        }

        return this.getSuccessResponseObject('updateKeysDone');
    };

    deleteKeysPersons = async (connection, table, version, key) => {
        const persons = await this.getPersons(connection, table, version);
        if (persons.isError) {
            return persons;
        }

        persons.forEach((item) => delete item[key]);

        for (const person of persons) {
            const resUpdatePerson = await this.updatePerson(
                connection,
                table,
                person.id,
                version,
                person
            );
            if (resUpdatePerson.isError) {
                return resUpdatePerson;
            }
        }

        return this.getSuccessResponseObject('keyDeleted');
    };

    writeLogs = async (dbConnection, table, logsRows) => {
        const isLogsTableExists = await this.isTableExists(
            dbConnection,
            `${table}_logs`
        );
        if (isLogsTableExists.isError) {
            return isLogsTableExists;
        }

        if (!isLogsTableExists) {
            const resCreateIfNotExistsLogs = await this.createIfNotExistsLogs(
                dbConnection,
                table
            );
            if (resCreateIfNotExistsLogs.isError) {
                return resCreateIfNotExistsLogs;
            }
        }

        const resUpdateLogs = await this.updateLogs(
            dbConnection,
            table,
            logsRows
        );
        if (resUpdateLogs.isError) {
            return resUpdateLogs;
        }

        return true;
    };

    broadcastEmails = async (dbConnection, table, logsRows) => {
        const isEmailTableExists = await this.isTableExists(
            dbConnection,
            `${table}_emails`
        );
        if (isEmailTableExists.isError) {
            return isEmailTableExists;
        }

        if (!isEmailTableExists) {
            const rescreateIfNotExistsEmails = await this.createIfNotExistsEmails(
                dbConnection,
                table
            );
            if (rescreateIfNotExistsEmails.isError) {
                return rescreateIfNotExistsEmails;
            }
        }

        const emails = await this.getEmails(dbConnection, table);
        if (emails.isError) {
            return emails;
        }

        const tableLogs = createTable(this.dom, logsRows);
        this.sendEmail(this.transporter, tableLogs, emails);

        return true;
    };

    getPersons = async (dbConnection, table, version) => {
        let res;

        try {
            [res] = await dbConnection.execute(
                `SELECT id, ${version} FROM ${table}`
            );
        } catch (err) {
            res = this.getErrorResponseObject('sqlError', err.message);
        }

        return res.isError
            ? res
            : res.map((item) => {
                  item[version] = JSON.parse(item[version]);
                  item[version].id = item.id;
                  return item[version];
              });
    };

    getPerson = async (dbConnection, personId, version, table) => {
        let res;

        try {
            [res] = await dbConnection.execute(
                `SELECT ${version} FROM ${table} WHERE id = ${personId}`
            );
        } catch (err) {
            res = this.getErrorResponseObject('sqlError', err);
        }

        if (res.isError) {
return res;
}

        const person = JSON.parse(res[0][version]);

        person.personId = personId;

        return person;
        // return res.isError ? res : ;
    };

    getLogs = async (dbConnection, table, version) => {
        let res;

        try {
            [res] = await dbConnection.execute(
                `SELECT * FROM ${table}_logs WHERE version='${version}'`
            );
        } catch (err) {
            res = this.getErrorResponseObject('sqlError', err);
        }

        return res;
    };

    getEmails = async (dbConnection, table) => {
        let res;

        try {
            [res] = await dbConnection.execute(
                `SELECT email FROM ${table}_emails`
            );
        } catch (err) {
            res = this.getErrorResponseObject('sqlError', err);
        }

        return res;
    };

    updatePerson = async (
        dbConnection,
        table,
        personId,
        version,
        personFields
    ) => {
        let res;

        try {
            [{ changedRows: res }] = await dbConnection.execute(
                `UPDATE ${table} SET ${version}='${JSON.stringify(
                    personFields
                )}' WHERE id=${personId}`
            );
        } catch (err) {
            res = this.getErrorResponseObject('sqlError', err);
        }

        return res;
    };

    updateLogs = async (dbConnection, table, logsRows) => {
        let res;
        const columnsStr = `(user, dateTime, version, searchingValue, changed, oldValue, newValue)`;
        const logsDataStr = logsRows
            .map(
                ({
                    user,
                    dateTime,
                    version,
                    searchingValue,
                    changedKey,
                    oldValue,
                    newValue,
                }) => {
                    return `('${user}', '${dateTime}', '${version}', '${searchingValue}', '${changedKey}','${oldValue}', '${newValue}')`;
                }
            )
            .join(', ');

        try {
            [{ affectedRows: res }] = await dbConnection.execute(
                `INSERT INTO ${table}_logs ${columnsStr} VALUES ${logsDataStr}`
            );
        } catch (err) {
            res = this.getErrorResponseObject('sqlError', err);
        }

        return res;
    };

    createIfNotExistsLogs = async (dbConnection, table) => {
        let res;
        const SQLQueryCreateTableLogs =
            `CREATE TABLE IF NOT EXISTS ${table}_logs` +
            `(user VARCHAR(16), dateTime VARCHAR(16), version VARCHAR(16), searchingValue VARCHAR(16),` +
            `changed VARCHAR(16), oldValue VARCHAR(64), newValue VARCHAR(64))`;

        try {
            await dbConnection.execute(SQLQueryCreateTableLogs);
        } catch (err) {
            res = this.getErrorResponseObject('sqlError', err.message);
        }

        return res.isError ? res : true;
    };

    createIfNotExistsEmails = async (queryArgs) => {
        let res;
        const SQLQueryCreateTableLogs = `CREATE TABLE IF NOT EXISTS ${queryArgs.table}_emails (id VARCHAR(16), email VARCHAR(64))`;

        try {
            await this.dbConnection.execute(SQLQueryCreateTableLogs);
        } catch (err) {
            res = this.getErrorResponseObject('sqlError', err.message);
        }

        return res.isError ? res : true;
    };

    sendEmail = (transporter, emailContent, emails) => {
        if (emails.length) {
            emails.forEach((item) => {
                const mailOptions = {
                    from: transporter.options.auth.user,
                    to: item.email,
                    subject: 'Logs changes',
                    html: emailContent,
                };
                transporter.sendMail(mailOptions, (err) => {
                    if (err) {
                        throw err;
                    }
                    console.log('Email sent!');
                });
            });
        }
    };

    getColumnsNames = async (dbConnection, table) => {
        let res;

        try {
            [res] = await dbConnection.execute(
                `SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = N'${table}'`
            );
        } catch (err) {
            res = this.getErrorResponseObject('sqlError', err.message);
        }

        return res.isError
            ? res
            : res
                  .map((column) => column.COLUMN_NAME)
                  .filter((name) => name !== 'id');
    };
}

const isProduction = process.env.NODE_ENV === 'production';
console.log(`Production status: ${isProduction}.`);

if (isProduction) {
    (async () => {
        const instance = new Server();
        await instance.initialize();
    })();
}
