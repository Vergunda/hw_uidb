import HtmlDiff from 'htmldiff-js';

export function getChangedValues(personData, personDataFromDB) {
    const changedData = [];
    for (let key in personData) {
        if (personData[key] != personDataFromDB[key]) {
            changedData.push({
                changedKey: key,
                oldValue: personDataFromDB[key],
                newValue: personData[key],
            });
        }
    }
    return changedData;
}

export function createTable(dom, logsRows) {
    const table = dom.window.document.createElement('table');
    const tableItemStyle = 'padding: 10px; border: 1px solid black;border-collapse: collapse;';
    table.setAttribute('style', tableItemStyle);
    table.appendChild(createHeadRow(dom, logsRows[0], tableItemStyle));
    createTableLogs(dom, table, logsRows, tableItemStyle);
    return table.outerHTML;
}

function createTableLogs(dom, table, logsRows, tableItemStyle) {
    logsRows.forEach(item => {table.appendChild(createRow(dom, item, tableItemStyle))});
}

function createHeadRow(dom, logsRow, tableStyleItem) {
    const thead = dom.window.document.createElement('thead');
        for (const key in logsRow) {
            if (key != 'oldValue' && key != 'newValue') {
                thead.appendChild(createCell(dom, key, tableStyleItem));
            }
        }
    thead.appendChild(createCell(dom, 'value', tableStyleItem))
    return thead;
}

function createRow(dom, logsRow, tableItemStyle) {
    const tr = dom.window.document.createElement('tr');
    for (const key in logsRow) {
        if (key != 'oldValue' && key != 'newValue') {
            tr.appendChild(createCell(dom, logsRow[key], tableItemStyle));
        }
    }
    tr.appendChild(createValueCell(dom, logsRow.oldValue, logsRow.newValue, tableItemStyle));
    return tr;
}

function createCell(dom, value, tableItemStyle) {
    const td = dom.window.document.createElement('td');
    td.textContent = value;
    td.setAttribute('style', tableItemStyle);
    return td;
}

function createValueCell(dom, oldValue, newValue, tableItemStyle) {
    const td = dom.window.document.createElement('td');
    td.setAttribute('style', tableItemStyle);
    let diff = HtmlDiff.execute(oldValue, newValue);
    diff = diff.replace('<del class="diffmod">', '<span style="text-decoration: line-through; background-color: #fbb6c2; color: #555;">');
    diff = diff.replace('</del>', '</span>');
    diff = diff.replace('<ins class="diffmod">', '<span style="background-color: #d4fcbc;">');
    diff = diff.replace('</ins>', '</span>');
    // td.innerHTML = `<span style="text-decoration: line-through; background-color: #fbb6c2; color: #555;">${oldValue}</span><span style="background-color: #d4fcbc;">${newValue}</span>`;
    td.innerHTML = diff;
    return td;
}

export function getDateTime() {
    return new Date().toLocaleString([], {day: '2-digit', month: '2-digit', year: '2-digit', hour: '2-digit', minute:'2-digit'})
}
