import actionTypes from "../constants/actionTypes";

export const sendRequest = payload => ({ type: actionTypes.SEND_REQUEST, payload });