import SettingsModal from './SettingsModal.jsx';
import { connect } from 'react-redux';
import * as actions from './actions';
import * as selectors from './selectors';

export const mapStateToProps = state => ({
    isActive: selectors.getSettingsModalStatus(state),
    editorFields: selectors.getEditorFields(state),
    settingsFields: selectors.getSettingsModalFields(state),
    settingsSelects: selectors.getSettingsModalSelects(state),
});

export const mapDispatchToProps = dispatch => ({
    hideSettingsModal: payload => dispatch(actions.hideSettingsModal(payload)),
    updateSettingsFields: payload => dispatch(actions.updateSettingsFields(payload)),
    updateSettingsSelects: payload => dispatch(actions.updateSettingsSelects(payload)),
    addEditorField: payload => dispatch(actions.addEditorField(payload)),
    editEditorField: payload => dispatch(actions.editEditorField(payload)),
    deleteEditorField: payload => dispatch(actions.deleteEditorField(payload)),
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(SettingsModal);