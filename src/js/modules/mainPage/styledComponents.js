import styled from 'styled-components';

const Container = styled.div`
    margin-left: auto;
    margin-right: auto;
    border: 1px solid #000;
    max-width: 1000px;
`;

const Content = styled.div`
    padding: 15px;
`;

export {
    Container,
    Content,
};