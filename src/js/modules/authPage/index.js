import AuthPage from './AuthPage.jsx';
import { connect } from 'react-redux';
import * as actions from './actions';
import * as selectors from './selectors';

export const mapStateToProps = state => ({
    userFields: selectors.getUserFields(state),
});

export const mapDispatchToProps = dispatch => ({
    login: payload => dispatch(actions.login(payload)),
    updateUserFields: payload => dispatch(actions.updateUserFields(payload)),
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(AuthPage);