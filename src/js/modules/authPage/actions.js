import ActionTypes from '../../constants/actionTypes';

export const login = payload => ({ type: ActionTypes.LOGIN, payload });
export const updateUserFields = payload => ({ type: ActionTypes.UPDATE_USER_FIELDS_STORE, payload });