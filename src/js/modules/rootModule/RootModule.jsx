import React from 'react';
import PropTypes from 'prop-types';
import AuthPage from '../authPage/';
import MainPage from '../mainPage/';
import EditPage from '../editPage/';
import SettingsModal from '../settingsModal/';
import { Layout } from './styledComponents';
import './rootModule.css';

export default class RootModule extends React.Component {
    getPage = () => {
        const { showAuthPage, showEditPage } = this.props;
    
        if (showAuthPage) return <AuthPage />;

        if (showEditPage) return <EditPage />;

        return <MainPage />;
    }

    render() {
        return (
            <React.Fragment>
                <Layout>
                    {this.getPage()}
                </Layout>
                <SettingsModal/>
            </React.Fragment>
        );
    }
}

RootModule.propTypes = {
    showAuthPage: PropTypes.bool.isRequired,
    showEditPage: PropTypes.bool.isRequired,
};