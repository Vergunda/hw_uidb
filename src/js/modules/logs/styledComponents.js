import styled from 'styled-components';

const TableBlock = styled.table`
    width: 100%;
    text-align: center;
`;

const Th = styled.th`
    border: 1px solid #000;
    padding: 10px;
    text-transform: uppercase;
`;

const Td = styled.td`
    border: 1px solid #000;
    padding: 8px 10px;
`;

TableBlock.Th = Th;
TableBlock.Td = Td;

export { TableBlock };