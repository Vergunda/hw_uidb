import React from 'react';
import PropTypes from 'prop-types';
import strings from '../../strings/en';
import { TabsBlock } from './styledComponents';

const Tabs = ({ tabs, toggleTabs }) => {
    const handlerClick = (event) => {
        toggleTabs(event.target.dataset.tab);
    };

    const tabComponents = Object.keys(tabs).map(key => {
        const text = strings.resources.tabs[key];
        
        if (tabs[key]) {
            return (
                <TabsBlock.Item.active
                    key={key}
                    data-tab={key}
                    onClick={handlerClick}>
                    {text}
                </TabsBlock.Item.active>
            );
        } 

        return (
            <TabsBlock.Item
                key={key}
                data-tab={key}
                onClick={handlerClick}>
                {text}
            </TabsBlock.Item>
        );
    });

    return (
        <TabsBlock>
            {tabComponents}
        </TabsBlock>
    );
};

Tabs.propTypes = {
    tabs: PropTypes.object.isRequired,
    toggleTabs: PropTypes.func.isRequired,
};

export default React.memo(Tabs);