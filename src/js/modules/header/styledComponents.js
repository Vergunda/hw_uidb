import styled from 'styled-components';

const HeaderBlock = styled.header`
    display: flex;
    justify-content: space-between;
    margin-bottom: 15px;
    margin-right: auto;
    margin-left: auto;
    max-width: 1000px;
`;

const Select = styled.select`
    padding: 6px 10px;
    border-color: black;
    outline: none;
`;

const Btn = styled.button`
    border: 1px solid #000;
    padding: 8px 15px;
    color: #000; 
    cursor: pointer;
    text-transform: uppercase;
    line-height: 15px;
    text-decoration: none;
    text-align: center;
`;

const gray = styled(Btn)`
    background-color:#ddd;
`;

Btn.gray = gray;

export { HeaderBlock, Select, Btn };