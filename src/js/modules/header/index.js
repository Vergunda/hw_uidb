import Header from './Header.jsx';
import { connect } from 'react-redux';
import * as actions from './actions';
import * as selectors from './selectors';

export const mapStateToProps = state => ({
    currentVersion: selectors.getCurrentVersion(state),
});

export const mapDispatchToProps = dispatch => ({
    logout: () => dispatch(actions.logout()),
    changeVersion: (payload) => dispatch(actions.changeVersion(payload)),
    showSettingsModal: () => dispatch(actions.showSettingsModal()),
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Header);