import React from 'react';
import PropTypes from 'prop-types';
import PureComponent from '../../base/PureComponent.jsx';
import strings from '../../strings/en';
import config from '../../config/config.js';
import { Editor, Btn } from './styledComponents';

export default class Editors extends PureComponent {
    componentDidMount() {
        this.props.fetchEditorsData();
    }

    handlerClick = (event) => {
        this.props.showEditPage({
            editorId: event.target.parentElement.id,
        });
    }

    getEditors = () => {
        const { editorsData, write } = this.props;
        const lastIndex = editorsData.length - 1;

        return editorsData.map((editor, index) => {
            const editorsContent = 
                <React.Fragment>
                    <Editor.Id>{editor.id}</Editor.Id>
                    <div>{editor[config.searchingValue]}</div>

                    {write &&
                        <Btn.editor onClick={this.handlerClick}>
                            {strings.resources.buttons.edit}
                        </Btn.editor>
                    }
                </React.Fragment>;

            if (lastIndex == index) {
                return (
                    <Editor.last
                        key={editor.id}
                        id={editor.id}>
                        {editorsContent}
                    </Editor.last>
                )
            }

            return (
                <Editor
                    key={index}
                    id={editor.id}>
                    {editorsContent}
                </Editor>
            );
        })
    }

    render() {
        return (
            <React.Fragment>
                {this.getEditors()}
            </React.Fragment>
        );
    }
}

Editors.propTypes = {
    write: PropTypes.bool.isRequired,
    editorsData: PropTypes.array.isRequired,
    fetchEditorsData: PropTypes.func.isRequired,
    showEditPage: PropTypes.func.isRequired,
};