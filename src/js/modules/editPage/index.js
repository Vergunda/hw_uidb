import EditPage from './EditPage.jsx';
import { connect } from 'react-redux';
import * as actions from './actions';
import * as selectors from './selectors';

export const mapStateToProps = state => ({
    editorFields: selectors.getEditorFields(state),
});

export const mapDispatchToProps = dispatch => ({
    saveEdits: (payload) => dispatch(actions.sendEdits(payload)),
    updateEditorFields: (payload) => dispatch(actions.updateEditorFields(payload)),
    updateEditPage: (payload) => dispatch(actions.updateEditPage(payload)),
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(EditPage);