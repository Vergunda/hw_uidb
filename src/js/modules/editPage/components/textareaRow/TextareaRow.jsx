import React from 'react';
import PropTypes from 'prop-types';
import { Form } from '../../styledComponents';

const TextareaRow = ({ label, name, value, updateEditorFields }) => {
    const handlerChange = (event) => {
        updateEditorFields({
            value: event.target.value,
            name: event.target.name,
        });
    };

    return (
        <Form.Row>
            <Form.Label>{label}</Form.Label>
            <Form.Textarea
                name={name}
                value={value}
                onChange={handlerChange}
                required/>
        </Form.Row>
    );
};

TextareaRow.propTypes = {
    label: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    value: PropTypes.string,
    updateEditorFields: PropTypes.func.isRequired,
};

export default React.memo(TextareaRow);