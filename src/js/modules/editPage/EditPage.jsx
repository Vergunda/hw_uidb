import React from 'react';
import PropTypes from 'prop-types';
import PureComponent from '../../base/PureComponent.jsx';
import ErrorBoundary from '../../decorators/ErrorBoundary.jsx';
import TextareaRow from './components/textareaRow/TextareaRow.jsx';
import { Form } from './styledComponents';
import strings from '../../strings/en';

@ErrorBoundary
export default class EditPage extends PureComponent {
    handlerClickClose = () => {
        this.props.updateEditPage({
            active: false
        });
    }

    handlerClickSave = () => {
        const { saveEdits, editorFields } = this.props;

        saveEdits({
            editorFields,
            merge: false,
        });
    }

    handlerClickSaveAndMerge = () => {
        const { saveEdits, editorFields } = this.props;

        saveEdits({
            editorFields,
            merge: true,
        });
    }

    getTextareaRows = () => {
        const { updateEditorFields, editorFields } = this.props;

        return Object.keys(editorFields).map(key => {
            if (key !== 'id' && key !== 'personId') {
                return (
                    <TextareaRow 
                        key={key}
                        name={key}
                        label={key}
                        value={editorFields[key]}
                        updateEditorFields={updateEditorFields}/>
                )
            }
        });
    }

    render() {
        return (
            <Form.Wrap>
                <Form>
                    {this.getTextareaRows()}
                    
                    <Form.RowBtn>
                        <Form.Btn.green onClick={this.handlerClickSave}>
                            {strings.resources.buttons.save}
                        </Form.Btn.green>
                        <Form.Btn.green onClick={this.handlerClickSaveAndMerge}>
                            {strings.resources.buttons.saveAndMerge}
                        </Form.Btn.green>
                        <Form.Btn.gray
                            onClick={this.handlerClickClose}
                            type='button'>
                            {strings.resources.buttons.cancel}
                        </Form.Btn.gray>
                    </Form.RowBtn>
                </Form>
            </Form.Wrap>
        )
    };
}

EditPage.propTypes = {
    editorFields: PropTypes.object.isRequired,
    updateEditorFields: PropTypes.func.isRequired,
    updateEditPage: PropTypes.func.isRequired,
    saveEdits: PropTypes.func.isRequired,
};