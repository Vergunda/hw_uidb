export default {
    base: 'http://localhost:1010',
    auth: '/auth',
    logout: '/logout',
    persons: '/persons',
    person: '/person',
    logs: '/logs',
    edit: '/edit',
    editAndMerge: '/edit-and-merge',
    addField: '/add-key',
    editField: '/update-key',
    deleteField: '/delete-key',
};