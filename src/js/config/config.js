export default {
    title: 'db users',
    version: 'version1',
    searchingValue: 'age',
    searchingValueColumn: 'Searching Val.',
    versionOptions: [
        'version1',
        'version2',
        'version3',
        'version4',
    ],
};
