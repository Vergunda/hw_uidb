export default Object.defineProperties({}, {
    // NEW
    SEND_REQUEST: { value: 'send_request', writable: false },

    LOGIN: { value: 'login', writable: false },
    LOGIN_SUCCESS: { value: 'login_success', writable: false },
    LOGIN_FAILURE: { value: 'login_failure', writable: false },
    LOGOUT: { value: 'logout', writable: false },
    LOGOUT_SUCCESS: { value: 'logout_success', writable: false },
    LOGOUT_FAILURE: { value: 'logout_failure', writable: false },
    UPDATE_AUTH_PAGE_STORE: { value: 'update_auth_page_store', writable: false },

    FETCH_EDITORS_DATA: { value: 'fetch_edirots_data', writable: false },
    FETCH_EDITORS_DATA_SUCCESS: { value: 'fetch_edirots_data_success', writable: false },
    FETCH_EDITORS_DATA_FAILURE: { value: 'fetch_edirots_data_failure', writable: false },
    UPDATE_EDITORS_DATA_STORE: { value: 'update_editors_data_store', writable: false },

    FETCH_EDIT_PAGE_DATA: { value: 'fetch_edit_page_data', writable: false },
    FETCH_EDIT_PAGE_DATA_SUCCESS: { value: 'fetch_edit_page_data_success', writable: false },
    FETCH_EDIT_PAGE_DATA_FAILURE: { value: 'fetch_edit_page_data_failure', writable: false },
    SEND_EDITS: { value: 'send_edits', writable: false },
    SEND_EDITS_SUCCESS: { value: 'send_edits_success', writable: false },
    SEND_EDITS_FAILURE: { value: 'send_edits_failure', writable: false },
    UPDATE_EDIT_PAGE_STORE: { value: 'update_edit_page_store', writable: false },

    FETCH_LOGS_DATA: { value: 'fetch_logs_data', writable: false },
    FETCH_LOGS_DATA_SUCCESS: { value: 'fetch_logs_data_success', writable: false },
    FETCH_LOGS_DATA_FAILURE: { value: 'fetch_logs_data_failure', writable: false },
    UPDATE_LOGS_DATA_STORE: { value: 'update_logs_data_store', writable: false },

    CHANGE_VERSION: { value: 'change_version', writable: false },
    UPDATE_VERSION_STORE: { value: 'update_version_store', writable: false },
    
    TOGGLE_SETTINGS_MODAL: { value: 'toggle_settings_modal', writable: false },
    TOGGLE_SETTINGS_MODAL_STORE: { value: 'toggle_settings_modal_store', writable: false },
    ADD_EDITOR_FIELD: { value: 'add_editor_field', writable: false },
    ADD_EDITOR_FIELD_SUCCESS: { value: 'add_editor_field_success', writable: false },
    ADD_EDITOR_FIELD_FAILURE: { value: 'add_editor_field_failure', writable: false },
    EDIT_EDITOR_FIELD: { value: 'edit_editor_field', writable: false },
    EDIT_EDITOR_FIELD_SUCCESS: { value: 'edit_editor_field_success', writable: false },
    EDIT_EDITOR_FIELD_FAILURE: { value: 'edit_editor_field_failure', writable: false },
    DELETE_EDITOR_FIELD: { value: 'delete_editor_field', writable: false },
    DELETE_EDITOR_FIELD_SUCCESS: { value: 'delete_editor_field_success', writable: false },
    DELETE_EDITOR_FIELD_FAILURE: { value: 'delete_editor_field_failure', writable: false },

    UPDATE_USER_FIELDS_STORE: { value: 'update_user_fields_store', writable: false },
    TOGGLE_TABS: { value: 'toggle_tabs', writable: false },
    UPDATE_TABS_STORE: { value: 'update_tabs_store', writable: false },
    UPDATE_EDITOR_FIELDS_STORE: { value: 'update_editor_fields_store', writable: false },
    UPDATA_SETTINGS_MODAL_FIELDS_STORE: { value: 'update_settings_modal_fields_store', writable: false },
    UPDATA_SETTINGS_MODAL_SELECTS_STORE: { value: 'update_settings_modal_selects_store', writable: false },
    RESET_SETTINGS_MODAL_SELECTS_STORE: { value: 'reset_settings_modal_select_store', writable: false },
    
    CONNECTION_FAILURE: { value: 'connection_failure', writable: false },
});
