import ActionTypes from '../../constants/actionTypes';

export default (state = {}, action) => {
    switch (action.type) {
        case ActionTypes.UPDATE_EDIT_PAGE_STORE:
            return {
                ...state,
                ...action.payload,
            };

        case ActionTypes.UPDATE_EDITOR_FIELDS_STORE:
            return { 
                ...state,
                editorFields: {
                    ...state.editorFields,
                    [action.payload.name]: action.payload.value,
                }
            };

        default: {
            return state;
        }
    }
};