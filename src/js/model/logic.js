import strings from '../strings/en';

export const toggleObjectFlags = (tabs, currentTab) => {
    const newTabs = { ...tabs };

    for (const tab in newTabs) {
        newTabs[tab] = false;
    }

    newTabs[currentTab] = true;

    return newTabs;
};

export const login = ({ id, isAuthorized, permissionWrite }) => {
    window.history.pushState({}, '', `?id=${id}`);

    return {
        active: !isAuthorized,
        write: permissionWrite,
    };
};

export const logout = async ({ isAuthorized }) => {
    window.history.replaceState({}, '', '/');

    return {
        active: !isAuthorized,
    };
};

export const showErrorMessage = ({ code, data }) => {
    alert(strings.resources[code], data);
};

export const getEditPageData = (status, data) => {
    return {
        active: !status,
        editorFields: data ? data : {},
    };
};

export const showSuccessMessage = (type) => {
    alert(type);
}